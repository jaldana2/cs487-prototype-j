<?php
// for repetitive stuff.

function addMarkersToGoogleMap($db) {
	$stmt = $db->prepare("SELECT * FROM locations");
	$res = $stmt->execute();
	
	$row = $res->fetchArray();
	echo "
		var image = 'images/flag.png';
		var image2 = 'images/flag2.png';
		var markerMyPos = new google.maps.Marker({
			position: {lat: m_pos[0], lng: m_pos[1]},
			map: map,
			title: \"My Position\"
		});
	";
	if ($row) {
		do {
			if ($row["disabled"] === 1) continue;
			$services = unserialize($row["services"]);
			$services_string = "<li>" . @implode("</li><li>", $services) . "</li>";
			
			$pointValid = true;
			if (isset($_GET["q"])) {
				$searchText = strtolower(" {$row['title']} {$row['description']} {$row['type']} {$row['services']}");
				foreach (explode(" ", $_GET["q"]) as $query) {
					$query = strtolower($query);
					if (!strpos($searchText, $query)) {
						$pointValid = false;
					}
				}
			}
			if ($pointValid) {
				echo "
				var marker{$row['id']} = new google.maps.Marker({
					position: {lat: {$row['lat']}, lng: {$row['lng']}},
					map: map,
					icon: image,
					title: \"{$row['title']}\"
				});
				";
			}
			else {
				echo "
				var marker{$row['id']} = new google.maps.Marker({
					position: {lat: {$row['lat']}, lng: {$row['lng']}},
					map: map,
					icon: image2,
					title: \"{$row['title']}\"
				});
				";
			}
			echo "
			marker{$row['id']}.addListener('click', function() {
				$.featherlight('Popup.php?action=infoPopup&i={$row['id']}');
				//infowindow{$row['id']}.open(map, marker{$row['id']});
			});
			";
		} while ($row = $res->fetchArray());
	}
}
function addUpDownvoteHandler() {
	echo "
		$('body').on('click', '.btn-vote', function(e) {
			$.featherlight.current().close();
			$.post('CastVote.php?i=' + $(this).attr('data-id') + '&vote=' + $(this).attr('data-vote-type')).done(function(data) {
				$.notify(data, { className: 'info' });
			});
		});
	";
}
?>