<?php
include "DatabaseStuff.php";

if (!isset($_GET["action"])) die("Invalid action");

switch ($_GET["action"]) {
	case "addLocationDialog":
		$lat = $_GET["lat"];
		$lng = $_GET["lng"];
		echo "
			<h3>Add New Location</h3>
			<form action='?do=SubmitFacts' method='post'>
				<input type='hidden' name='lat' value='{$lat}' />
				<input type='hidden' name='lng' value='{$lng}' />
				<div class='form-group'>
					<input type='text' class='form-control' name='title' placeholder='Title' />
				</div>
				<div class='form-group'>
					<input type='text' class='form-control' name='type' placeholder='Type of Place' />
				</div>
				<div class='form-group'>
					<textarea class='form-control' name='description' placeholder='Description' row='4'></textarea>
				</div>
				<div class='form-group'>
					<input type='checkbox' name=\"service[ATM]\" /> ATM
				</div>
				<div class='form-group'>
					<input type='checkbox' name=\"service[Bathroom]\" /> Bathroom
				</div>
				<div class='form-group'>
					<input type='checkbox' name=\"service[WaterFountain]\" /> Water Fountain
				</div>
				<div class='form-group'>
					<input type='checkbox' name=\"service[OpenToPublic]\" checked /> Open to general public
				</div>
				<div class='form-group'>
					<input type='submit' value='Save' />
				</div>
			</form>
		";
	break;
	case "infoPopup":
		$stmt = $db->prepare("SELECT * FROM locations WHERE id = :id");
		$stmt->bindValue('id', $_GET["i"], SQLITE3_INTEGER);
		$res = $stmt->execute();
		
		$row = $res->fetchArray();
		echo "
			<div class='container' style='min-width: 300px; max-width: 600px; margin: 0; padding: 5px;'>
		";
		if ($row["reported"] === 1) {
			echo "
				<h3>{$row['title']}</h3>
				<b>{$row['type']}</b>
				<small>{$row['description']}</small>
				<hr />
				<ul class='fa fa-ul'>
			";
			foreach (unserialize($row["services"]) as $service) {
				echo "<li><i class='fa fa-check fa-li'></i> {$service}</li>";
			}
			echo "
				</ul>
				<br />
				<span class='label label-danger'>This location has been reported.</span>
			";
		}
		else {
			echo "
				<h3>{$row['title']}</h3>
				<b>{$row['type']}</b>
				<small>{$row['description']}</small>
				<hr />
				<div class='row'>
					<div class='col-xs-6 btn btn-xs btn-primary btn-vote' style='text-align: center;' data-id='{$row['id']}' data-vote-type='up'><i class='fa fa-thumbs-up'></i> +{$row['upvote']}</div>
					<div class='col-xs-6 btn btn-xs btn-primary btn-vote' style='text-align: center;' data-id='{$row['id']}' data-vote-type='up'>-{$row['downvote']} <i class='fa fa-thumbs-down'></i></div>
				</div>
				<br />
				<ul class='fa fa-ul'>
			";
			if (count(unserialize($row["services"])) > 0) foreach (unserialize($row["services"]) as $service) {
				echo "<li><i class='fa fa-check fa-li'></i> {$service}</li>";
			}
			echo "
				</ul>
				<br />
				<a href='?do=ReportError&i={$_GET['i']}' class='label label-primary'>Report Error</a>
			";
		}
		echo "
			</div>
		";
	break;
}
?>