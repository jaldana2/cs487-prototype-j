<?php
$db = new SQLite3("location.db");
$init = "
	CREATE TABLE IF NOT EXISTS locations (
		id INTEGER PRIMARY KEY,
		title STRING,
		type STRING,
		services STRING,
		upvote INTEGER,
		downvote INTEGER,
		lat STRING,
		lng STRING,
		reported INTEGER,
		reported_reason STRING,
		disabled INTEGER,
		description STRING
	);
";
$db->exec($init);
?>