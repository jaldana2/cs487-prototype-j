<?php
include "DatabaseStuff.php";
session_start();

if (!isset($_SESSION["rating_history"])) {
	$_SESSION["rating_history"] = array();
}
if (isset($_SESSION["rating_history"][$_GET["i"]])) {
	echo "You have already rated this location.";
	die();
}

if (isset($_GET["i"]) && isset($_GET["vote"])) {
	$stmt = $db->prepare("SELECT * FROM locations WHERE id = :id;");
	$stmt->bindValue('id', $_GET["i"], SQLITE3_INTEGER);
	$res = $stmt->execute();
	$row = $res->fetchArray();
	
	if ($_GET["vote"] === "up") {
		$stmt = $db->prepare("UPDATE locations SET upvote = :value WHERE id = :id;");
		$stmt->bindValue('id', $_GET["i"], SQLITE3_INTEGER);
		$stmt->bindValue('value', $row["upvote"] + 1, SQLITE3_INTEGER);
		$res = $stmt->execute();
	}
	elseif ($_GET["vote"] === "down") {
		$stmt = $db->prepare("UPDATE locations SET downvote = :value WHERE id = :id;");
		$stmt->bindValue('id', $_GET["i"], SQLITE3_INTEGER);
		$stmt->bindValue('value', $row["downvote"] + 1, SQLITE3_INTEGER);
		$res = $stmt->execute();
	}
	
	$_SESSION["rating_history"][$_GET["i"]] = true; // temporary block
	
	echo "Your rating has been saved.";
}
?>