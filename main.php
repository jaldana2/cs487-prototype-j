<?php
include "config.php";
include "GoogleyStuff.php";

// ready databases
include "DatabaseStuff.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo APP_NAME; ?></title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet' />
	<link href='https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/yeti/bootstrap.min.css' rel='stylesheet' />
	<link href='https://cdnjs.cloudflare.com/ajax/libs/featherlight/1.4.0/featherlight.min.css' rel='stylesheet' />
	<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/featherlight/1.4.0/featherlight.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
</head>
<body>
	<?php
	$page = "Home";
	if (isset($_GET["do"])) $page = $_GET["do"];
	
	if ($page === "Home") {
		echo "
			<div class=\"box-header\">
				<h1 class=\"box-title\">" . APP_NAME . "</h1>
				<a href=\"?do=Help\" class=\"btn\">Help</a>
			</div>
		";
	}
	else {
		echo "
			<div class=\"box-header-mini\">
				<h1 class=\"box-title\">" . APP_NAME . "</h1>
				<a href=\"?do=Home\" class=\"btn-alt\">Return</a>
			</div>
		";
	}
	
	switch ($page) {
		case "Admin":
			include "Admin.html";
		break;
		case "Home":
			include "Home.html";
		break;
		case "StartExploring":
			include "StartExploring.html";
		break;
		case "SubmitFacts":
			if (isset($_POST["title"])) include "_AddNewLocation.php";
			include "SubmitFacts.html";
		break;
		case "Preferences":
			include "Preferences.html";
		break;
		case "Help":
			include "Help.html";
		break;
		case "ReportError":
			include "ReportError.html";
		break;
		case "Debug":
			include "DebugGPS.html";
		break;
		default:
			echo "Unknown page: {$page}";
		break;
	}
	
	include "Debug.html";
	?>
	<div class="box-footer">
		<small>CS487 Project &mdash; Team J</small>
		<br />
		<small><a href='./devEmu.html' target='_parent'>Desktop</a> | <a href='./main.php' target='_parent'>Mobile</a></small>
	</div>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcDHdEPQdE9D0vItjLIHtM3GWyj1_wuyw&callback=initMap" async defer></script>
	<script>
		/* javascript config */
		var debug = false;
		
		/* geolocation */
		var x = document.getElementById("loc-debug");
		var m_pos = [41.8357659, -87.6266945]; // set default to near state and 33rd st.
		var usingGeolocation = false;
		function getLocation() {
			console.log("getLocation called");
			console.log(navigator.geolocation);
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(showPosition);
			} else {
				x.innerHTML = "Geolocation is not supported by this browser.";
			}
		}
		function showPosition(position) {
			m_pos = [position.coords.latitude, position.coords.longitude];
			x.innerHTML = "Latitude: " + position.coords.latitude + 
			"<br>Longitude: " + position.coords.longitude; 
			console.log(position);
			console.log(m_pos);
			usingGeolocation = true;
			<?php
				if ($page === "StartExploring" || $page === "Home" || $page === "SubmitFacts") {
					echo '$.notify("Geolocation acquired successfully.", { className: "success" });';
				}
			?>
		}
		
		if (typeof initMap == 'undefined') {
			function initMap() {
				// placeholder
				console.log('called placeholder initMap()');
			}
		}
		
			
		$(function(e) {
			if (debug) {
				$("#debugSpace").css("display", "block"); // show debug div
				console.log("Debug enabled");
			}
			getLocation();
		});
	</script>
</body>
</html>