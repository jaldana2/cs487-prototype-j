<?php
include "config.php";
include "GoogleyStuff.php";

// ready databases
include "DatabaseStuff.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo APP_NAME; ?></title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet' />
	<link href='https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/yeti/bootstrap.min.css' rel='stylesheet' />
	<link href='https://cdnjs.cloudflare.com/ajax/libs/featherlight/1.4.0/featherlight.min.css' rel='stylesheet' />
	<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/featherlight/1.4.0/featherlight.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
</head>
<body>
	<?php
	$x = 0;
	while (isset($_COOKIE['notif'.$x])){
		if ($_COOKIE['notif'.$x] != '0') {
			// figure out id from title
			$idFromTitle = -1;
			$stmt = $db->prepare("SELECT * FROM locations WHERE title = :title;");
			$stmt->bindValue("title", substr($_COOKIE["notif{$x}"], strlen("Landmark: ")), SQLITE3_TEXT);
			$res = $stmt->execute();
			
			$row = $res->fetchArray();
			
			if ($row) {
				$idFromTitle = $row["id"];
			}
			echo "
			<div class=\"notif\">
				<a href=\"main.php?do=StartExploring&id={$idFromTitle}\" class=\"big\">" . $_COOKIE['notif'.$x] . "</a>
				</div> <br/>
			";
		}
		$x = $x + 1;
	}
	
	?>
</body>
</html>