<?php
	$valid = true;
	foreach ($_POST as $k => $v) {
		if (!is_array($v) && strlen($v) === 0) {
			// something's empty.
			echo "<script>alert('Something is missing (key={$k}) - Please try again.');</script>";
			$valid = false;
		}
	}
	if ($valid) {
		$stmt = $db->prepare("INSERT INTO locations (title, type, services, upvote, downvote, lat, lng, description) VALUES (:title, :type, :services, :upvote, :downvote, :lat, :lng, :desc)");
		$stmt->bindValue("title", $_POST["title"], SQLITE3_TEXT);
		$stmt->bindValue("type", $_POST["type"], SQLITE3_TEXT);
		$stmt->bindValue("services", serialize(array_keys($_POST["service"])), SQLITE3_TEXT);
		$stmt->bindValue("upvote", 1, SQLITE3_INTEGER);
		$stmt->bindValue("downvote", 0, SQLITE3_INTEGER);
		$stmt->bindValue("lat", $_POST["lat"], SQLITE3_TEXT);
		$stmt->bindValue("lng", $_POST["lng"], SQLITE3_TEXT);
		$stmt->bindValue("desc", $_POST["description"], SQLITE3_TEXT);
		
		$stmt->execute();
	}
?>