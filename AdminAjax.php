<?php
include "DatabaseStuff.php";

$stmt = $db->prepare("UPDATE locations SET " . $_POST["type"] . " = :value WHERE id = :id;");
$stmt->bindValue('id', $_POST["id"], SQLITE3_INTEGER);
if ($_POST["type"] === "Disabled") {
	$stmt->bindValue('value', $_POST["value"], SQLITE3_INTEGER);
}
else {
	$stmt->bindValue('value', $_POST["value"], SQLITE3_TEXT);
}
$res = $stmt->execute();
?>